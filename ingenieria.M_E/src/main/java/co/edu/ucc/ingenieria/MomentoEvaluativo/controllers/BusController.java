/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.ucc.ingenieria.MomentoEvaluativo.controllers;

import co.edu.ucc.ingenieria.MomentoEvaluativo.model.Bus;
import co.edu.ucc.ingenieria.MomentoEvaluativo.services.BusService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 57301
 */
@RestController
@RequestMapping("/Bus")
public class BusController {
    @Autowired
    private BusService busservice;
    
    
    @GetMapping("/")
    public ResponseEntity<List<Bus>> queryAll(){
        List<Bus> bs= busservice.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(bs);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Bus> queryById(@PathVariable Long id){
        Bus b= busservice.getById(id);
        return ResponseEntity.ok().body(b);
    }
    
    
    @GetMapping("/search{nombre}")
    public ResponseEntity<List<Bus>> queryByName(@PathVariable String matricula){
        List<Bus> bs= busservice.getByName(matricula);
        return ResponseEntity.ok().body(bs);   
    }
    @PostMapping("/")
    public ResponseEntity<Boolean> saveBus(@Valid @RequestBody Bus bus){
        busservice.save(bus);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}

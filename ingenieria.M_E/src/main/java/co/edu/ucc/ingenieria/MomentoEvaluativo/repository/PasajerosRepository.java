/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.ucc.ingenieria.MomentoEvaluativo.repository;

import co.edu.ucc.ingenieria.MomentoEvaluativo.model.Pasajeros;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author 57301
 */
public interface PasajerosRepository extends JpaRepository<Pasajeros, Long> {
    @Query("Select p From Pasajeros p Where nombre LIKE ?1")
    public List<Pasajeros> getByName(String nombre);
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.ucc.ingenieria.MomentoEvaluativo.repository;

import co.edu.ucc.ingenieria.MomentoEvaluativo.model.Bus;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author 57301
 */
@Repository
public interface BusRepository extends JpaRepository<Bus, Long> {
    @Query("Select b From Bus b WHERE matricula LIKE ?1")
    public List<Bus> getByName(String matricula);
}

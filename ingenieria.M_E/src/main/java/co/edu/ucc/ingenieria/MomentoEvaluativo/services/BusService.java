/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.ucc.ingenieria.MomentoEvaluativo.services;

import co.edu.ucc.ingenieria.MomentoEvaluativo.model.Bus;
import co.edu.ucc.ingenieria.MomentoEvaluativo.repository.BusRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 57301
 */
@Service
public class BusService {
    @Autowired
    private BusRepository busrepository;
    
    public List<Bus> getAll() {
        return busrepository.findAll();
    }
    
    public List<Bus> getByName(String matricula){
        return busrepository.getByName("%"+matricula+"%");
    }
    
    public Bus getById(Long id){
        return busrepository.getReferenceById(id);
    }
    
    public void save (Bus b){
        busrepository.save(b);
    }
    
    public void delete(Long id){
        busrepository.deleteById(id);
    }
}

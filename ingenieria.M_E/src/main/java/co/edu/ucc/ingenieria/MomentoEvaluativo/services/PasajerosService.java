/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.ucc.ingenieria.MomentoEvaluativo.services;

import co.edu.ucc.ingenieria.MomentoEvaluativo.model.Pasajeros;
import co.edu.ucc.ingenieria.MomentoEvaluativo.repository.PasajerosRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 57301
 */
@Service
public class PasajerosService {
    @Autowired
    private PasajerosRepository pasajerosrepository;
    
    public List<Pasajeros> getAll(){
        return pasajerosrepository.findAll();
    }
    
    public List<Pasajeros> getByName(String nombre){
        return pasajerosrepository.getByName("%"+nombre+"%");
    }
    
    public Pasajeros getById(Long id){
        return pasajerosrepository.getReferenceById(id);
    }
    public void save (Pasajeros p){
        pasajerosrepository.save(p);
    }
    public void delete (Long id){
        pasajerosrepository.deleteById(id);
    }
}

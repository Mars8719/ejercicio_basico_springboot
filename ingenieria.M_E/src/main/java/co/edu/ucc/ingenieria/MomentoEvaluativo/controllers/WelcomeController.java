/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.ucc.ingenieria.MomentoEvaluativo.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author 57301
 */
public class WelcomeController {
     @GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    public String welcome(){
        return ""
                +"<html>"
                +"<body>"
                +"<h1> Bienvenidos al REST de proyectos </h1>"
                +"<p> Las rutas se encuentran en /proyectos/</p>"
                +"</body>"
                +"</html>";
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.ucc.ingenieria.MomentoEvaluativo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;

import javax.persistence.ManyToOne;

import javax.persistence.Entity;

import lombok.Data;


/**
 *
 * @author 57301
 */
@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Pasajeros {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull(message="Nombre del pasajero es obligatorio")
    private String nombre;
    @NotNull(message="El apellido del pasajero es obligatorio")
    private String apellido;
    @NotNull(message="El numero de la cedula del pasajero es obligatorio")
    private Long cedula;
    @NotNull(message="El telefono movil es obligatoria")
    @Pattern(regexp="\\d{10}", message = "El numero de telefono movil debe tener 10 digitos")
    private Long telefono;
    @NotNull(message="El correo electronico es obligatorio")
    private String correo;
    
    @ManyToOne
    private Bus bus;
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.ucc.ingenieria.MomentoEvaluativo.controllers;

import co.edu.ucc.ingenieria.MomentoEvaluativo.model.Pasajeros;
import co.edu.ucc.ingenieria.MomentoEvaluativo.services.PasajerosService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 57301
 */
@RestController
@RequestMapping("/Pasajeros")
public class PasajerosController {
    @Autowired
    private PasajerosService pasajerosservice;
    
    @GetMapping("/")
    public ResponseEntity<List<Pasajeros>> queryAll(){
        List<Pasajeros> ps= pasajerosservice.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(ps);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Pasajeros> queryById(@PathVariable Long id){
        Pasajeros p=pasajerosservice.getById(id);
        return ResponseEntity.ok().body(p);
    }
    
    @GetMapping("/search/{nombre}")
    public ResponseEntity<List<Pasajeros>>queryByName(@PathVariable String nombre){
        List<Pasajeros>ps = pasajerosservice.getByName(nombre);
        return ResponseEntity.ok().body(ps);
    }
    @PostMapping("/")
        public ResponseEntity<Boolean> savePasajeros(@Valid @RequestBody Pasajeros pasajeros){
        pasajerosservice.save(pasajeros);
        return ResponseEntity.status(HttpStatus.CREATED).body(Boolean.TRUE);
    }
        
}

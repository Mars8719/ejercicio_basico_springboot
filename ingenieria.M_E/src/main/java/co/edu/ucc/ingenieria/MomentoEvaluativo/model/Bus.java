/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.ucc.ingenieria.MomentoEvaluativo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author 57301
 */
@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Bus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull(message="El numero de la matricula es obligatorio")
    private String matricula;
    @NotNull(message="El tipo de motor es obligatorio")
    @Enumerated(EnumType.STRING)
    private Tipomotor motor;
    @NotNull(message="la Capacidad es obligatoria")
    private Integer capacidad;
    
    
    public enum Tipomotor{
        Gasolina,diesel
    }
    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    @OneToMany(mappedBy="bus")
    private List<Pasajeros> pasajeros;
}
